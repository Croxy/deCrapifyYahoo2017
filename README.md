# Make Yahoo Great (again?)!!!

Every year I play fantasy on Yahoo Sports. Every year I want to gouge my eyes out looking at all their daily fantasy, and fantasy shop, and "CARCOMPANY BEST TEAM OF THE WEEK!" crap.

So, I took to using TamperMonkey to modify the UI to get it a little better for me. 

Here is this year's version. 

You can install the user script for tamper monkey by navigating here: https://openuserjs.org/scripts/the_umm_guy/decrapifyYahooFF

Or, if you know how, do it manually.

Follow these instructions for installing if you have any issues: https://github.com/OpenUserJs/OpenUserJS.org/wiki/Tampermonkey-for-Chrome

Please report any issues you might have!

